#!/usr/bin/perl -w

use warnings;
use strict;
use DBI;

my $dsn = "dbi:SQLite:dbname=test.db";
my $dbh = DBI->connect($dsn, '','', { 
                            AutoCommit => 0, RaiseError => 1,
                        });

foreach my $v (@ARGV) {
    if (!eval {
	if ($v eq 'bw') { $dbh->begin_work or die; }
	elsif ($v eq 'rb') { $dbh->rollback or die; }
	elsif ($v eq 'ci') { $dbh->commit or die; }
	elsif ($v eq 'dc') { $dbh->disconnect or die; }
	elsif ($v eq 'ct') { $dbh->do('CREATE TABLE t (f TEXT)') or die; }
	elsif ($v eq 'i') { $dbh->do('INSERT INTO t VALUES ("x")') or die; }
	else { die; }
	1;
    }) {
	warn $@;
    }
}
